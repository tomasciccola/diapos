const { app, BrowserWindow, ipcMain:ipc } = require('electron')
const path = require('path')
const fs = require('fs')

const createWindow = () => {
  const win = new BrowserWindow({
    width:1920,
    height:1080,
    fullscreen:true,
    autoHideMenuBar: true, 
    webPreferences: {
      contextIsolation:false,
      nodeIntegration:true
    }
  })
  win.loadFile('public/index.html')
}

app.whenReady().then(() => {
  createWindow()
})

let dataDir = app.getPath('userData') 
let configFile = path.join(dataDir,'config.json')
/*
 * config schema
 * {
 * imagenes, ttransicion, tslide
 * }
 */
const initObj = {
  rutaImg:'', 
  ttransicion: 1.0,
  tslide: 5,
  nimagenes:10
}
const resolveConfig = () => {
  if(!fs.existsSync(dataDir)){
    fs.mkdirSync(dataDir)
  }
  if(!fs.existsSync(configFile)){
    fs.writeFileSync(configFile, JSON.stringify(initObj, null, 4))
  }
  return fs.readFileSync(configFile).toString()
}

//IPC
ipc.on('getConfig', (event, _) => {
  event.reply('config', resolveConfig())
})

ipc.on('updateConfig', (e, data) => {
  fs.writeFileSync(configFile, data)
})
