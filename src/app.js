const {ipcRenderer:ipc} = require('electron')
const fs = window.require('fs')
const path = window.require('path')
const SLIDE_A = document.querySelector('#slide0')
const SLIDE_B = document.querySelector('#slide1')
const SLIDES = [SLIDE_A,SLIDE_B]
let at = 0
let atLayer = 0
let nVideos = 0

let config = {
  rutaImg: '',
  ttransicion: 0.5,
  nimagenes:10
}

const sortByCreationTime =  (a,b) => {
  const aStat = fs.statSync(`${config.rutaImg}/${a}`)
  const bStat = fs.statSync(`${config.rutaImg}/${b}`)
  return new Date(bStat.birthtime).getTime() - new Date(aStat.birthtime).getTime();
}

const filterVideos = f => path.extname(f) === '.mp4' || path.extname(f) === '.mkv'

const changeLayer = files => () => {
  nVideos = files.length
  SLIDES[atLayer].classList.add('hidden')
  atLayer = (atLayer + 1) % 2
  SLIDES[atLayer].classList.remove('hidden')
  SLIDES[atLayer].src=`file://${config.rutaImg}/${files[at]}`
  at = (at + 1) % (config.nimagenes > files.length ? files.length : config.nimagenes)
}

const nextSlide = () => {
  const files = fs.readdirSync(config.rutaImg)
    .filter(filterVideos)
    .sort(sortByCreationTime)

  // reset when adding video to folder
  if(files.length !== nVideos){
    at = 0
    console.log('new!')
    setTimeout(changeLayer(files), 2000)
  }else{
    changeLayer(files)()
  } 

  console.log('at', at)
  console.log('layer', atLayer)


}

const startSlideShow = () => {
  slides.forEach(slide => {
    slide.style.transition = `opacity ${config.ttransicion}s ease-in`
    slide.addEventListener('ended', _ => {
      console.log('terminé')
      nextSlide()
    })
  })
  form.classList.add('hidden')
  nextSlide()
}

//CONFIG
const slides = document.querySelectorAll('.slide')
const ttransicion = document.querySelector('#ttransicion')
const nimagenes = document.querySelector('#nimagenes')
const form = document.querySelector('#config')
form.addEventListener('submit', e => {
  e.preventDefault()
  var form = e.currentTarget
  var data = new FormData(form)                                       // 2.
  var body = {}
  for (var pair of data.entries()) body[pair[0]] = pair[1]            // 4.
  config.ttransicion = parseFloat(body.ttransicion)
  config.nimagenes = parseInt(body.nimagenes)
  ipc.send('updateConfig', JSON.stringify(config, null, 4))
  startSlideShow()
})

const input = document.querySelector('#rutaImg')
input.addEventListener('change', function(){
  config.rutaImg = path.dirname(this.files[0].path)
})

ipc.send('getConfig')
ipc.on('config', (_,c) => {
  config = JSON.parse(c)
  ttransicion.value = config.ttransicion
  nimagenes.value = config.nimagenes
  if(config.rutaImg.length !== 0){
    startSlideShow()
  }
})



